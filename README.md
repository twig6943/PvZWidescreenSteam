A launcher for the PvZ Widescreen to be playable through Steam.

How to use this:

1 - Install Plants vs Zombies through Steam

2 - Open file location of the game

3 - Extract the archive you've downloaded earlier to pvz's root folder 

4 - Now all you need to do is click play from Steam, have fun!

(Bass.dll isn't included in this fork for obvious reasons)

![GameSelector](/screenshots/GameSelector.png)

![SurvivalDay](/screenshots/SurvivalDay.png)

![ZenGarden](/screenshots/ZenGarden.png)

